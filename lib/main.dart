import 'package:flutter/material.dart';
import 'package:myyda/screens/landing.dart';
import 'package:myyda/screens/newShop.dart';
import 'package:myyda/screens/profile.dart';

void main() => runApp(Main());

class Main extends StatefulWidget {
  Main({Key key}) : super(key: key);

  _MainState createState() => _MainState();
}

class _MainState extends State<Main> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          home: Scaffold(
            body: Landing(),
          )),
    );
  }
}
