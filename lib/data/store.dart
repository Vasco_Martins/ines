import 'package:cloud_firestore/cloud_firestore.dart';

class Shop {
  final String shopID;
  final String ownerID;
  final String shopName;
  final String shopLocation;
  final List products;
  final bool isVerified;
  final String profileLink;
  final List followers;
  final int trustRate;
  final List opinions;
  final int sells;
  final String photoURL;

  Shop(
      {this.shopID,
      this.ownerID,
      this.shopName,
      this.shopLocation,
      this.products,
      this.isVerified,
      this.profileLink,
      this.followers,
      this.trustRate,
      this.opinions,
      this.sells,
      this.photoURL});

  factory Shop.fromFirestore(DocumentSnapshot doc) {
    Map data = doc.data;

    return Shop(
      shopID: doc.documentID,
      ownerID: data['ownerID'] ?? '',
      shopName: data['shopName'] ?? '',
      shopLocation: data['shopLocation'] ?? '', // <-- this will be the address
      products: data['products'] ?? '',
      isVerified: data['isVerified'] ?? false,
      profileLink: data['profileLink'] ?? '',
      followers: data['followers'] ?? '',
      trustRate: data['trustRate'] ?? 0,
      opinions: data['opinions'] ?? '',
      sells: data['sellsNum'] ?? 0,
      photoURL: data['photoURL'] ?? '',
    );
  }

  factory Shop.fromMap(Map data) {
    return Shop(
      shopID: data['shopID'] ?? '',
      ownerID: data['ownerID'] ?? '',
      shopName: data['shopName'] ?? '',
      shopLocation: data['shopLocation'] ?? '', // <-- this will be the address
      products: data['products'] ?? '',
      isVerified: data['isVerified'] ?? false,
      profileLink: data['profileLink'] ?? '',
      followers: data['followers'] ?? '',
      trustRate: data['trustRate'] ?? 0,
      opinions: data['opinions'] ?? '',
      sells: data['sellsNum'] ?? 0,
      photoURL: data['photoURL'] ?? '',
    );
  }
}
