import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:myyda/data/user.dart';

class DatabaseService {
  
  final Firestore _db = Firestore.instance;

  Future<User> getUser(String uid) async {
    var snap = await _db.collection('users').document(uid).get();

    return User.fromMap(snap.data);
  }

  Future<User> getShop(String shopID) async {
    var snap = await _db.collection('shops').document(shopID).get();

    return User.fromMap(snap.data);
  }

  Future<User> getOwnerShops(String shopOwner) async {
    var snap = await _db.collection('shops').where("ownerID ===" + shopOwner).getDocuments();

    return User.fromMap(snap.documents.asMap());
  }
}
