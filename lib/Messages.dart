class Messages {
  static const String logInTitle = "Login to continue";
  static const String registerTitle = "Register to continue";
  static const String loginButton = "LOGIN";
  static const String registerButton = "REGISTER";
  static const String noAccount = "Dont you have an account?";
  static const String hasAccount = "Already have an account?";

  static const String formErrorInvalidEmail = "Please introduce a valid e-mail";
  static const String formErrorPasswordEmpty = "Introduce a password";
  static const String formErrorInvalidUsername = "You username must not contain spaces";
  static const String formErrorUsernameAlreadyExistss = "Your username already exists";

  static const double buttonFontSize = 15;
} 