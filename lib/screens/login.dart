import 'package:flutter/material.dart';
import 'package:myyda/screens/home.dart';
import 'package:myyda/screens/register.dart';
import 'package:myyda/screens/splash.dart';
import 'package:myyda/utils/validations.dart';
import '../Messages.dart';
import '../utils/Auth.dart';

import 'package:firebase_auth/firebase_auth.dart';

class Login extends StatefulWidget {
  Login({Key key}) : super(key: key);

  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formKey = GlobalKey<FormState>();

  final FirebaseAuth _auth = FirebaseAuth.instance;

  bool _login = true;

  String _email;
  String _password;

  final txtEmail = TextEditingController();
  final pswPassword = TextEditingController();

  final Auth _authService = Auth();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    txtEmail.dispose();
    pswPassword.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.pop(context, false),
              color: Colors.black,
            ),
            elevation: 0.0,
            backgroundColor: Colors.transparent),
        body: ListView(children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Column(
              children: <Widget>[
                Text(
                  Messages.logInTitle,
                  style: TextStyle(fontSize: 30, color: Colors.blueAccent),
                ),
                buildLogin(),
              ],
            ),
          ),
        ]),
      ),
    );
  }

  Form buildLogin() {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(labelText: "Email"),
            validator: Validations.validateEmail,
            keyboardType: TextInputType.emailAddress,
            onSaved: (String val) => _email = val,
            controller: txtEmail,
          ),
          SizedBox(
            height: 10.0,
          ),
          TextFormField(
            decoration: InputDecoration(
              labelText: "Password",
            ),
            obscureText: true,
            validator: Validations.validatePasswordLogin,
            onSaved: (String val) => _password = val,
            controller: pswPassword,
          ),
          SizedBox(
            height: 10.0,
          ),
          FlatButton(
            onPressed: () {
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (BuildContext context) => Register()));
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  Messages.noAccount,
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    Messages.registerButton,
                    style: TextStyle(
                      color: Colors.green,
                    ),
                  ),
                ),
              ],
            ),
          ),
          RaisedButton(
            onPressed: () {
              login();
            },
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)),
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Text(
                Messages.loginButton,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            color: Colors.green,
          )
        ],
      ),
    );
  }

  void login() {
    if (_formKey.currentState.validate()) {
      _auth
          .signInWithEmailAndPassword(
              email: txtEmail.text, password: pswPassword.text)
          .then((data) {

            _authService.updateUserData(data);
            Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (BuildContext context) => SplashScreen()),);})
          .catchError((e) => showDialog(
              context: context,
              builder: (BuildContext builder) {
                return AlertDialog(
                  title: Text("Error"),
                  content: Text(e.message),
                );
              }));
    }
  }
}
