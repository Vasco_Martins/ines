import 'package:flutter/material.dart';
import 'package:myyda/screens/profile.dart';

import 'home.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super(key: key);

  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(
        Duration(seconds: 4),
        () => {
              //Navigator.push(context, new MaterialPageRoute(builder: (BuildContext context) => Home()))
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (BuildContext context) => Profile()))
            });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: DecoratedBox(
          decoration: BoxDecoration(
            color: Colors.black,
            image: DecorationImage(
              image: AssetImage('assets/images/logo-white.gif'),
              fit: BoxFit.contain,
            ),
          ),
        ),
      ),
    );
  }
}
