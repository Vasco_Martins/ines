import 'package:flutter/material.dart';
import 'package:myyda/screens/login.dart';
import 'package:myyda/screens/splash.dart';
import 'package:myyda/utils/validations.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:myyda/utils/Auth.dart';
import 'package:myyda/Messages.dart';

class Register extends StatefulWidget {
  Register({Key key}) : super(key: key);

  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final _formKey = GlobalKey<FormState>();

  final FirebaseAuth _auth = FirebaseAuth.instance;
  final _authService = Auth();

  bool _login = true;
  String _email;
  String _password;
  String _username;

  final txtEmail = TextEditingController();
  final pswPassword = TextEditingController();
  final txtUsername = TextEditingController();

  String _role = "Buyer";
  bool isSwitched = false;
  bool value = false;

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    txtEmail.dispose();
    pswPassword.dispose();
    txtUsername.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.pop(context, false),
              color: Colors.black,
            ),
            elevation: 0.0,
            backgroundColor: Colors.transparent),
        body: ListView(children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Column(
              children: <Widget>[
                Text(
                  Messages.registerTitle,
                  style: TextStyle(fontSize: 30, color: Colors.blueAccent),
                ),
                buildRegister(),
              ],
            ),
          ),
        ]),
      ),
    );
  }

  Form buildRegister() {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(labelText: "Username"),
            validator: Validations.validateUsername,
            keyboardType: TextInputType.text,
            onSaved: (String val) => _username = val,
            controller: txtUsername,
          ),
          TextFormField(
            decoration: InputDecoration(labelText: "Email"),
            validator: Validations.validateEmail,
            keyboardType: TextInputType.emailAddress,
            onSaved: (String val) => _email = val,
            controller: txtEmail,
          ),
          SizedBox(
            height: 10.0,
          ),
          TextFormField(
            decoration: InputDecoration(
              labelText: "Password",
            ),
            obscureText: true,
            validator: Validations.validatePasswordLogin,
            onSaved: (String val) => _password = val,
            controller: pswPassword,
          ),
          SizedBox(
            height: 10.0,
          ),
          FlatButton(
            onPressed: () {
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (BuildContext context) => Login()));
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  Messages.hasAccount,
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    Messages.loginButton,
                    style: TextStyle(
                      color: Colors.green,
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 5.0,
          ),
          Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 80.0, right: 10.0),
              child: Text(
                'Buyer',
                style: TextStyle(
                  color: Colors.grey[650],
                ),
              ),
            ),
            /* SizedBox(width: 90.0), */
            Switch(
              value: value,
              onChanged: (bool val) => _handleChange(val),
              activeTrackColor: Colors.blueAccent,
              activeColor: Colors.lightBlue,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 80.0),
              child: Text(
                'Seller',
                style: TextStyle(
                  color: Colors.grey[650],
                ),
              ),
            ),
          ]),
          SizedBox(
            height: 10.0,
          ),
          RaisedButton(
            onPressed: () {
              register();
            },
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)),
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Text(
                Messages.registerButton,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            color: Colors.green,
          )
        ],
      ),
    );
  }

  void _handleChange(bool val) {
    setState(() {
      if (val) {
        _role = "Seller";
        value = true;
        val = true;
      } else {
        _role = "Buyer";
        value = false;
        val = false;
      }
    });
  }

  void register() {
    if (_formKey.currentState.validate()) {
      _auth
          .createUserWithEmailAndPassword(
              email: txtEmail.text, password: pswPassword.text)
          .then((data) {
        _authService.updateUserData(data);

        // Let's just insert our freshly registered friend
        // in our database and give him a role (buyer / seller)
        _authService.db
            .collection('users')
            .document(data.uid)
            .updateData({'displayName': txtUsername.text, 'role': _role});
        Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (BuildContext context) => SplashScreen()));
      }).catchError((e) => showDialog(
              context: context,
              builder: (BuildContext builder) {
                return AlertDialog(
                  title: Text("Error"),
                  content: Text(e.message),
                );
              }));
    }
  }
}
